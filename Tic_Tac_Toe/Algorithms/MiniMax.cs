﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using Tic_Tac_Toe.Models;
using Tic_Tac_Toe.Models.Normal.Enums;
using Tic_Tac_Toe.Models.Normal;


namespace Tic_Tac_Toe.Algorithms
{
   public class MiniMax
    {
        CellSymbol computer;
        CellSymbol human;


        public MiniMax(CellSymbol computer, CellSymbol player)
        {
            this.computer = computer;
            this.human = player;
        }
       
        public CellLocation FindBestMove(CellSymbol[,] board)
        {
            int bestValue = int.MinValue;
            CellLocation bestLocation  = new CellLocation(-1,-1);

            for(int i=0;i<board.GetLength(0);i++)
            {
                for(int j=0;j<board.GetLength(1);j++)
                {
                    if(board[i,j] == CellSymbol.empty)
                    {
                        board[i, j] = computer;
                        int move = ProcessMiniMax(board, false); //now let ssee what can human play if the computer played this move 
                        board[i, j] = CellSymbol.empty;
                        if(move > bestValue)
                        {
                            bestValue = move;
                            bestLocation = new CellLocation(i, j);
                        }
                    }
                }
            }

            return bestLocation;
        }

        private int ProcessMiniMax(CellSymbol[,] board, bool isMax)
        {
            var currentMoveWinner = evaluate(board);

            if(currentMoveWinner != 0)
                return currentMoveWinner;

            if(currentMoveWinner == 0)
            {
                bool thereIsNoAnotherMove = true ;

                foreach(CellSymbol cell in board)
                {
                    if (cell == CellSymbol.empty)
                        thereIsNoAnotherMove = false;
                }

                if(thereIsNoAnotherMove)
                    return 0;                     
            }




            if(isMax) //which mean computer turn
            {
                int bestScore = int.MinValue;
                for (int i = 0; i < board.GetLength(0); i++)
                {
                    for (int j = 0; j < board.GetLength(1); j++)
                    {
                        if (board[i, j] == CellSymbol.empty)
                        {
                            board[i, j] = computer;
                            bestScore = Math.Max(bestScore, ProcessMiniMax(board, false));
                            board[i, j] = CellSymbol.empty;
                        }
                    }
                }
                return bestScore;
            }
            else
            {
                int bestScore = int.MaxValue;
                for (int i = 0; i < board.GetLength(0); i++)
                {
                    for (int j = 0; j < board.GetLength(1); j++)
                    {
                        if(board[i,j] == CellSymbol.empty)
                        {
                            board[i, j] = human;
                            bestScore = Math.Min(bestScore, ProcessMiniMax(board, true));
                            board[i, j]  = CellSymbol.empty;
                        }
                    }
                }
                return bestScore;
            }


        }

        //+10 computer win , -10 human win , 0 no one
        int evaluate(CellSymbol[,] board)
        {
            int humanScore = -10;
            int computerScore = 10;
            int tie = 0;

            for (int i = 0; i < board.GetLength(0); i++)
            {
                bool IsHumanWinner = true;
                bool IsComputerWinner = true;
                for (int j = 0; j < board.GetLength(1); j++)
                {
                    if (board[i, j] != computer)
                        IsComputerWinner = false;

                    if (board[i, j] != human)
                        IsHumanWinner = false;
                }
                if (IsHumanWinner)
                    return humanScore;

                if (IsComputerWinner)
                    return computerScore;
            }


            for (int i = 0; i < board.GetLength(0); i++)
            {
                bool IsHumanWinner = true;
                bool IsComputerWinner = true;
                for (int j = 0; j < board.GetLength(1); j++)
                {
                    if (board[j, i] != computer)
                        IsComputerWinner = false;

                    if (board[j, i] != human)
                        IsHumanWinner = false;
                }
                if (IsHumanWinner)
                    return humanScore;

                if (IsComputerWinner)
                    return computerScore;
            }

            {
                bool IsHumanWinner = true;
                bool IsComputerWinner = true;
                for (int j = 0; j < board.GetLength(1); j++)
                {
                    if (board[j, j] != computer)
                        IsComputerWinner = false;

                    if (board[j, j] != human)
                        IsHumanWinner = false;
                }
                if (IsHumanWinner)
                    return humanScore;

                if (IsComputerWinner)
                    return computerScore;
            }


            {
                bool IsHumanWinner = true;
                bool IsComputerWinner = true;
                for (int j = 0; j < board.GetLength(1); j++)
                {
                    if (board[j, (board.GetLength(1) - 1)-j ] != computer)
                        IsComputerWinner = false;

                    if (board[j, (board.GetLength(1) - 1) - j] != human)
                        IsHumanWinner = false;
                }
                if (IsHumanWinner)
                    return humanScore;

                if (IsComputerWinner)
                    return computerScore;
            }


            return tie;            
        }
           
    }
}
