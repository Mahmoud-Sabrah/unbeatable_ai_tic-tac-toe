﻿using System;
using System.Drawing;
using Tic_Tac_Toe.Algorithms;
using Tic_Tac_Toe.Models.Normal;
using Tic_Tac_Toe.Models.Normal.Enums;

namespace Tic_Tac_Toe
{
    class Program
    {
        static void Main(string[] args)
        {            
            Board game = new Board(Models.Normal.Enums.CellSymbol.o,Models.Normal.Enums.CellSymbol.x);
            MiniMax AI = new MiniMax(Models.Normal.Enums.CellSymbol.o, Models.Normal.Enums.CellSymbol.x);




            while(true)
            {
                MoveResponse result;
                do
                {
                    Console.Write("Enter the player row:");
                    var row = int.Parse(Console.ReadLine()) - 1;
                    Console.Write("Enter the player column:");
                    var column = int.Parse(Console.ReadLine())-1;
                    result = game.Move(game.Human, new CellLocation(row, column));
                    Console.WriteLine(result.ToString());
                } while (result != MoveResponse.Done);


                var gameResult = game.getGameResult();
                if (gameResult == GameResult.HumanWon || gameResult == GameResult.ComputerWon)
                {

                    Console.WriteLine(gameResult.ToString());
                    printBoard(game);
                    Console.ReadLine();
                    return;
                }



                //var list = game.getAvailableMoves();
                //game.Move(game.Computer, list[0].lastMove);

                game.Move(game.Computer, AI.FindBestMove(game.GameBoard));


                printBoard(game);

                gameResult = game.getGameResult();
                if (gameResult == GameResult.HumanWon || gameResult == GameResult.ComputerWon)
                {

                    Console.WriteLine(gameResult.ToString());
                    printBoard(game);
                    Console.ReadLine();
                    return;
                }



            }

        }


        public static void printBoard(Board game)
        {
            Console.WriteLine();
            for(int i=0;i<game.GameBoard.GetLength(0);i++)
            {
                for(int j=0;j<game.GameBoard.GetLength(1);j++)
                {
                    if(game.GameBoard[i,j] == CellSymbol.empty)                    
                        Console.Write(" " );                    
                    else 
                        Console.Write(game.GameBoard[i, j] );

                    if (j != game.GameBoard.GetLength(1) - 1)
                        Console.Write("|");
                }
                Console.WriteLine();

                if (i != game.GameBoard.GetLength(0) - 1)
                    Console.WriteLine("-----");
            }

        }

    }
}
