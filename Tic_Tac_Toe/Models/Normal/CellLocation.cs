﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tic_Tac_Toe.Models.Normal
{
   public struct CellLocation
    {
        public int row;
        public int column;
        public CellLocation(int row,int column)
        {
            this.row = row;
            this.column = column;
        }
    }
}
