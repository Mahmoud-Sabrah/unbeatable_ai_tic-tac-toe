﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using Tic_Tac_Toe.Models.Normal.Enums;

namespace Tic_Tac_Toe.Models.Normal
{
    public class Board
    {
        public CellSymbol[,] GameBoard { get; set; }
        public CellSymbol Computer { get; set; }
        public CellSymbol Human { get; set; }
        public CellSymbol lastPlayer { get; set; }
        public CellLocation lastMove { get; set; }


        public Board(CellSymbol Computer,CellSymbol Human)
        {
            GameBoard = new CellSymbol[3,3];
            this.Computer = Computer;
            this.Human = Human;
        }

        public Board(CellSymbol[,] GameBoard, CellSymbol Computer, CellSymbol Human)
        {
            this.GameBoard = GameBoard;
            this.Computer = Computer;
            this.Human = Human;
        }

        public MoveResponse Move(CellSymbol Player,CellLocation To)
        {
            if ((To.row > GameBoard.GetLength(0) -1 || To.row <0) ||
               (To.column > GameBoard.GetLength(1)-1 || To.column < 0)
                )
            {
                return MoveResponse.OutOfRange;
            }

            if (!CheckMoveAvailability(To))            
                return MoveResponse.CellAlreadyUsed;
            
            
            if (Player == Computer)
            {
                GameBoard[To.row, To.column] = Player;
                this.lastPlayer = Computer;
                this.lastMove = To;
                return MoveResponse.Done;
            }
            else if(Player == Human )
            {
                GameBoard[To.row, To.column] = Player;
                this.lastPlayer = Player;
                this.lastMove = To;
                return MoveResponse.Done;
            }
            return MoveResponse.Done;
        }

        public List<Board> getAvailableMoves()
        {
            List<Board> states = new List<Board>();
            for (int i = 0; i < GameBoard.GetLength(0); i++)
            {
                for (int j = 0; j < GameBoard.GetLength(1); j++)
                {
                    if (GameBoard[i, j] == CellSymbol.empty)
                    {
                        CellSymbol[,] copiedArray = new CellSymbol[GameBoard.GetLength(0), GameBoard.GetLength(1)];
                        Array.Copy(GameBoard, copiedArray, GameBoard.Length);

                        Board newState = new Board(copiedArray, Computer, Human);
                        newState.Move(Computer, new CellLocation(i, j));
                        states.Add(newState);
                    }
                }
            }
            return states;
        }

        public bool CheckMoveAvailability(CellLocation cell)
        {
            if (GameBoard[cell.row, cell.column] != CellSymbol.empty)
                return false;
            return true;
        }

        public GameResult getGameResult()
        {
            int row = lastMove.row;
            int column = lastMove.column;

            List<CellLocation> collected_col = new List<CellLocation>();                
            for(int i=0;i<GameBoard.GetLength(1);i++)
            {
                collected_col.Add(new CellLocation(row, i));
            }

            if (checkLine(collected_col, lastPlayer))
                return lastPlayer == Computer ? GameResult.ComputerWon : GameResult.HumanWon;


            List<CellLocation> collected_row = new List<CellLocation>();
            for (int i = 0; i < GameBoard.GetLength(0); i++)
            {
                collected_row.Add(new CellLocation(i, column));
            }
            if (checkLine(collected_row, lastPlayer))
                return lastPlayer == Computer ? GameResult.ComputerWon : GameResult.HumanWon;

            if(row == column)
            {
                List<CellLocation> collected_d1 = new List<CellLocation>();
                for (int i = 0; i < GameBoard.GetLength(0); i++)
                {
                    collected_d1.Add(new CellLocation(i, i));
                }
                if (checkLine(collected_d1, lastPlayer))
                    return lastPlayer == Computer ? GameResult.ComputerWon : GameResult.HumanWon;
            }

            if (row == (GameBoard.GetLength(1) - (column + 1)))
            {
                List<CellLocation> collected_d1 = new List<CellLocation>();
                for (int i = 0; i < GameBoard.GetLength(1); i++)
                {
                    collected_d1.Add(new CellLocation(i, GameBoard.GetLength(1) - (i+1)));
                }
                if (checkLine(collected_d1, lastPlayer))
                    return lastPlayer == Computer ? GameResult.ComputerWon : GameResult.HumanWon;
            }

            for(int i=0;i<GameBoard.GetLength(0);i++)
            {
                for(int j= 0;j<GameBoard.GetLength(1);j++)
                {
                    if (GameBoard[i, j] == CellSymbol.empty)
                        return GameResult.NoOne;
                }                
            }
            


            return GameResult.Draw;
        }

        bool checkLine(List<CellLocation> cells,CellSymbol Player)
        {
            foreach (CellLocation cell in cells)
                if (GameBoard[cell.row, cell.column] != Player)
                    return false;

            return true;
        }
        

}
}
