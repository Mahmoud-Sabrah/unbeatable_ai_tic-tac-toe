﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tic_Tac_Toe.Models.Normal.Enums
{
    public enum GameResult
    {
        ComputerWon,HumanWon,Draw, NoOne
    }
}
